# README #

## Host PC environment

 Graphic card : GeForce GTX 1080Ti
 
 Graphic driver : nvidia graphic driver 410.104

## Build docker image

 $ docker build --tag decenter-gpu-base:1.0 .

## Run base container

You can run the DECENTER gpu base container with the following command.

 $ docker run --runtime=nvidia -itd -p 5000:5000 --name decenter-gpu-base decenter-base:1.0
 
If you have already run the container, you can start or stop the container with the following command.

 $ docker start decenter-gpu-base
 
 $ docker stop decenter-gpu-base
 
While the contaner is running, you can attach to the container

 $ docker exec -it decenter-gpu-base /bin/bash


